let services_list_item = document.querySelectorAll('.services_list-item');
let services_list_title = document.querySelectorAll('.services_list-title');

services_list_item.forEach(function (element) {
    element.addEventListener('click', function () {

        const itemAttribute = element.getAttribute("data-title")
        services_list_item.forEach(function (elem) {
            elem.classList.remove('active')
        })

        services_list_title.forEach(function (text) {
            text.classList.remove('active')

            const textAttribute = text.getAttribute("data-text")
            if (itemAttribute === textAttribute) {

                element.classList.add('active')
                text.classList.add('active')
            };
        });
    });
});